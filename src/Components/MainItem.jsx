import React from 'react';
import '../App.css';

const MainItem = (props) => {
    return (
        <div className="main__item">
            <div className="main__image">
                {props.thumbnail === "self" || props.thumbnail === "default"
                    ? <img src="https://bitcoinexchangeguide.com/wp-content/uploads/2018/07/Reddits-Rival-ThunderMessage-Focuses-on-Quality-of-Content-through-Voting.jpg" alt="pic" />
                    : <img src={props.thumbnail} alt="pic" />}
            </div>
            <div className="main__text">
                <h2 className="item__title">{props.title}</h2>
                <p className="item__number">Number of comments {props.num_comments}</p>
                <a href={`https://www.reddit.com/${props.permalink}`} className="item__permalink" >Link</a>
            </div>
        </div>
    )
};

export default MainItem;