import React, { useState, useEffect } from 'react';
import './nullStyles.css';
import './App.css';
import MainItem from './Components/MainItem';
import Axios from 'axios';

const  App = () => {
  let [loading, setLoading] = useState(true)
  let [item, setItems] = useState(null);
  let [refresh, setrefresh] = useState(false);
  let [timer, setTimer] = useState(null);
  let [minComments, setMinCommetns] = useState(0);

  useEffect(() => {
    Axios.get('https://www.reddit.com/r/reactjs.json?limit=100').then(response => {
      setItems(response.data.data.children);
      setLoading(false);
    });
  }, []);

  const startRefresh = () => {
    setrefresh(true);
    setTimer(setInterval(() => {
      Axios.get('https://www.reddit.com/r/reactjs.json?limit=100').then(response => {
        setItems(response.data.data.children)
      });
    }, 3000))
  }

  const stopRefresh = () => {
    clearInterval(timer);
    setrefresh(false)
  }

  const updateMinCommetns = (event) => {
    setMinCommetns(event.target.value)
  }

  let sortItem = item ? item.sort((a, b) => b.data.num_comments - a.data.num_comments).filter(item => item.data.num_comments >= minComments) : null;

  return (
    <div className="App">
      <header className="header">
        <h1 className="header__title">Top commented</h1>
        <div className="header__button">
          {refresh
            ? <button onClick={stopRefresh}>Stop auto-refresh</button>
            : <button onClick={startRefresh}>Start auto-refresh</button>}
        </div>
        <div className="header__filter">
          <span> Current filter: {minComments}</span>
          <input type="range" onChange={updateMinCommetns} value={minComments} min={0} max={500} />
        </div>
      </header>
      <main className={"main__wrapper"}>
        {loading
          ? <p className="main__loading">loading...</p>
          : sortItem.length > 0
            ? sortItem.map(item => <MainItem key={item.data.id} permalink={item.data.permalink} title={item.data.title} thumbnail={item.data.thumbnail} num_comments={item.data.num_comments} />)
            : <p className="main__result">No results found matching your criteria</p>}
      </main>
    </div>
  );
}

export default App;
